// ==UserScript==
// @name          YouNow - Remove Clutter
// @version       0.0.1
// @description   Remove junk from YouNow pages, such as a footer, notification banner, recommendations, or sidebar content.
// @author        Bruce Bentley
// @copyright     2018, Bruce Bentley (https://github.com/brucebentley)
// @license       MIT; https://opensource.org/licenses/MIT
// @namespace     https://github.com/brucebentley
// @include       http://*.younow.com/*
// @include       https://*.younow.com/*
// @noframes
// @require       https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// @run-at        document-idle
// @grant         GM_registerMenuCommand
// @grant         GM_getValue
// @grant         GM_setValue
// @grant         GM_addStyle
// @icon          https://cdn.younow.com/favicon.ico?v=2
// @updateURL     https://raw.githubusercontent.com/brucebentley/userscripts/master/younow-remove-clutter.user.js
// @downloadURL   https://raw.githubusercontent.com/brucebentley/userscripts/master/younow-remove-clutter.user.js
// ==/UserScript==

/* eslint-env jquery */
('use strict');

const $ = window.$;
this.$ = this.jQuery = jQuery.noConflict(true);

const globalElements = {
  navBar: '.navbar',
  premiumMessages: '.premium-overlay',
  superMessages: '.supermessage-container',
  systemMessages: '.system-message',
  trendingUsers: '.trending-users'
};

const styles = [
  'background-color: #f8d7da',
  'border: 2px solid #f5c6cb',
  'border-radius: 4px',
  'color: #c63c3c',
  'display: flex',
  'flex: 0 1 auto',
  'font-size: 12px',
  'padding: 3px 60px;',
  'position: relative',
  'line-height: 24px',
  'width: 100%'
].join(';');

const $main = $('body #app');
const actions = {
  hideGlobalElements() {
    for (const elem of Object.keys(globalElements)) {
      $main.find(globalElements[elem]).remove();
    }
  }
};

for (const key of Object.keys(actions)) {
  actions[key]();
}

console.log('%c YouNow has been cleaned up!', styles);
