// ==UserScript==
// @name          Flickr - Blur Images
// @version       0.0.1
// @description   Blurs images on your user's feed & photostream by default, with an option to toggle the blur on/off.
// @author        Bruce Bentley
// @copyright     2018, Bruce Bentley (https://github.com/brucebentley)
// @license       MIT; https://opensource.org/licenses/MIT
// @namespace     https://github.com/brucebentley
// @include       http://*.flickr.com/*
// @include       https://*.flickr.com/*
// @noframes
// @run-at        document-idle
// @grant         GM_registerMenuCommand
// @grant         GM_getValue
// @grant         GM_setValue
// @grant         GM_addStyle
// @icon          https://s.yimg.com/pw/favicon.ico
// @updateURL     https://raw.githubusercontent.com/brucebentley/userscripts/master/flickr-blur-images.user.js
// @downloadURL   https://raw.githubusercontent.com/brucebentley/userscripts/master/flickr-blur-images.user.js
// ==/UserScript==

(function() {
  'use strict';

  const D = document;
  const H = D.head;
  const s = D.createElement('style');
  s.setAttribute('type', 'text/css');
  s.appendChild(
    D.createTextNode(
      '.feed-page-view img, .photo-list-photo-view.awake { filter: none; } .feed-page-view img.blurred, .photo-list-photo-view.awake.blurred { filter: blur(60px) opacity(50%); } .button__toggle-blur { background-color: rgba(248, 75, 75, 1); padding: 8px 15px !important; font-size: 14px; line-height: 1; }'
    )
  );
  H.appendChild(s);

  const b = D.createElement('li');
  const a = D.createElement('a');
  const c = D.createTextNode('Toggle Image Blur');
  a.appendChild(c);
  b.appendChild(a);
  a.setAttribute('class', 'gn-title button__toggle-blur');
  a.setAttribute('id', 'buttonToggleImageBlur');
  D.querySelector('.global-nav-container .nav-menu').appendChild(b);

  function toggleBlur() {
    const images = document.querySelectorAll('.feed-page-view img, .photo-list-photo-view.awake');
    for (let i = 0; i < images.length; i++) {
      images[i].classList.toggle('blurred');
    }
  }

  a.addEventListener('click', function(event) {
    toggleBlur();
  }, false);

})();
