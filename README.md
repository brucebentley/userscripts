# User Scripts

User scripts used to add and/or extend the functionality of various sites.

## Installation

1. Make sure you have user scripts enabled in your browser (these instructions refer to the latest versions of the browser):
   * Firefox - install [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/).
   * Chrome - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=chrome).
   * Opera - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=opera) or [Violent Monkey](https://addons.opera.com/en/extensions/details/violent-monkey/).
   * Safari - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=safari).
   * Dolphin - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=dolphin).
   * UC Browser - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=ucweb).
2. Get information or install:
   * Learn more about the userscript by clicking on the named link. You will be taken to the specific wiki page.
   * Install a script directly from GitHub by clicking on the "install" link in the table below.
   * Install a script from [GreasyFork](https://greasyfork.org/en/users/214047-bruce-bentley) `(GF)` from the userscript site page
   * Or, install the scripts from [OpenUserJS](hhttps://openuserjs.org/users/brucebentley/scripts) `(OU)`.

<br>

| Userscript Wiki                         | Direct Install     | Sites                       | Created    | Updated    | Notes     |
|-----------------------------------------|:------------------:|:---------------------------:|:----------:|:----------:|:----------|
| [Flickr - Blur Images][fbi-wiki]        | [Install][fbi-raw] | [GF][fbi-gf] · [OU][fbi-ou] | 2018.10.29 | 2018.10.29 |           |
| [YouNow - Remove Clutter][yrc-wiki]     | [Install][yrc-raw] | [GF][yrc-gf] · [OU][yrc-ou] | 2018.10.29 | 2018.10.29 |           |

[fbi-wiki]:https://github.com/brucebentley/userscripts/wiki/Flickr-Blur-Images
[yrc-wiki]: https://github.com/brucebentley/userscripts/wiki/YouNow-Remove-Clutter

[fbi-raw]: https://raw.githubusercontent.com/brucebentley/userscripts/master/flickr-blur-images.user.js
[yrc-raw]: https://raw.githubusercontent.com/brucebentley/userscripts/master/younow-remove-clutter.user.js

[fbi-gf]: https://greasyfork.org/en/scripts/373750-flickr-blur-images
[yrc-gf]: https://greasyfork.org/en/scripts/373751-younow-remove-clutter

[fbi-ou]: https://openuserjs.org/scripts/brucebentley/Flickr_-_Blur_Images
[yrc-ou]: https://openuserjs.org/scripts/brucebentley/YouNow_-_Remove_Clutter

<br>

## Updating

Userscripts are set up to automatically update. You can check for updates from within the Greasemonkey or Tampermonkey menu,
or click on the install link again to get the update.

Each individual userscript's change log is contained on its individual wiki page.

## Issues

Please report any userscript issues within this repository's [issue section](https://github.com/brucebentley/userscripts/issues).
Greasyfork messages are also received, but not as easily tracked. Thanks!

## Other Useful Userscripts:

* [GitHub userscripts](https://github.com/Mottie/GitHub-userscripts)
* [GitHub Dark Script](https://github.com/StylishThemes/GitHub-Dark-Script)
  * [Github Monospace Font Toggle](https://greasyfork.org/en/scripts/18787-github-monospace-font-toggle) _(also part of GitHub Dark Script)_
  * [GitHub Diff File Toggle](https://greasyfork.org/en/scripts/18788-github-diff-file-toggle) _(also part of GitHub Dark Script)_
  * [GitHub Toggle Code Wrap](https://greasyfork.org/en/scripts/18789-github-toggle-code-wrap) _(also part of GitHub Dark Script)_
* [GitHub Custom Emojis](https://github.com/StylishThemes/GitHub-Custom-Emojis)
* [GitHub Make Tooltips](https://greasyfork.org/en/scripts/22194)
* [Bitbucket userscripts](https://bitbucket.org/brucebentley/bitbucket-userscripts)
* [GitLab userscript](https://gitlab.com/brucebentley/GitLab-userscripts)
