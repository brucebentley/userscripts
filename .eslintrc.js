module.exports = {
    extends: 'eslint:recommended',
    env: {
        es6: true,
        browser: true,
        greasemonkey: true
    },
    parserOptions: {
        ecmaVersion: 2017
    },
    rules: {
        'arrow-spacing': 2,
        'brace-style': 1,
        camelcase: 2,
        curly: 1,
        'dot-location': [2, 'property'],
        eqeqeq: 1,
        indent: ['error', 2],
        'linebreak-style': [2, 'unix'],
        'no-array-constructor': 2,
        'no-console': 0,
        'no-else-return': 1,
        'no-empty': 0,
        'no-eval': 2,
        'no-multiple-empty-lines': ['error', { max: 2, maxEOF: 1 }],
        'no-octal': 2,
        'no-unused-vars': 1,
        'no-var': 1,
        'no-with': 2,
        quotes: [
            2,
            'single',
            {
                allowTemplateLiterals: true,
                avoidEscape: true
            }
        ],
        radix: 2,
        'spaced-comment': 2,
        semi: [2, 'always']
    }
};
